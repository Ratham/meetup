import gov.nasa.jpf.jvm.Verify;

public class Simple3
{
  public static void main(String [] args)
  {
    int x = 0,y = 1;
    boolean error = (args.length > 0 && args[0].equals("--error"));
    boolean bchoice = Verify.getBoolean();

    if(bchoice) {
      System.out.println("considering TRUE choice !!");
      x = Verify.getInt(-1,+1);
      
      if(error) y = x;
      else y = x + 1;
    } else {
      System.out.println("considering FALSE choice !!");
    }

    System.out.print("x=");
    System.out.print(x);
    System.out.print(" and y=");
    System.out.println(y);

    assert(x == y - 1);
  }
}
