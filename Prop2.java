import gov.nasa.jpf.jvm.Verify;

public class Prop2
{
  static int lockCount = 0;
  
  static void Lock() { ++lockCount; }

  static void Unlock() { --lockCount; }
 
  public static void main(String [] args)
  {
    boolean error = (args.length > 0) && args[0].equals("--error");

    boolean c1 = Verify.getBoolean();

    if(c1) { Lock(); Unlock(); Unlock(); Lock(); Lock(); }

    boolean c2 = error ? Verify.getBoolean() : c1;
    if(c2) Unlock();
    return;
  }
}
