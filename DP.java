public class DP
{
  /** a fork -- just provides an object */
  static protected class Fork { 
    public int id;
    public Fork(int i) { id = i; }
  }

  /** a philosopher */
  static protected class Phil extends Thread
  {
    /** the two forks and id */
    public Fork leftFork,rightFork;
    public int id;
    public Phil(int i,Fork lf,Fork rf) { id = i; leftFork = lf; rightFork = rf; }

    public void run()
    {
      while(true) {
        synchronized(leftFork) {
          synchronized(rightFork) {
            /** eat */
          }
        }
      }
    }
  }

  /** the main method */
  public static void main(String [] args)
  {
    if(args.length > 0) {
      int n = Integer.parseInt(args[0]);
      Fork [] forks = new Fork [n];
      Phil [] phils = new Phil [n];
      for(int i = 0;i < n;++i) forks[i] = new Fork(i);
      for(int i = 0;i < n;++i) phils[i] = new Phil(i,forks[i],forks[(i + 1) % n]);
      for(int i = 0;i < n;++i) phils[i].start();
    } else System.out.println("usage: jpf DP <philosopher num>"); 
  }
}
