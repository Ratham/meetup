import java.io.PrintWriter;
import java.io.StringWriter;

import gov.nasa.jpf.*;
import gov.nasa.jpf.Search;
import gov.nasa.jpf.jvm.*;
import gov.nasa.jpf.jvm.bytecode.*;

/**
 * property class to check for proper locking and unlocking
 */
public class LockUnlockProperty extends GenericProperty {
  
  public LockUnlockProperty (Config conf) {
  }
  
  public String getErrorMessage () {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    pw.print("lock-unlock property violated:\n");
   
    return sw.toString();
  }

  public boolean check (VM vm, Object o) {
    JVM jvm=(JVM) vm;
    StaticArea sa = jvm.getKernelState().sa;

    StaticElementInfo sei = sa.get("Prop2");

    int lockCount = sei.getIntField("lockCount");
    System.out.print("lockCount="); 
    System.out.println(lockCount);
    return (lockCount >= 0 && lockCount <= 1);
  }
}
