import java.io.PrintWriter;
import java.io.StringWriter;

import gov.nasa.jpf.*;
import gov.nasa.jpf.Search;
import gov.nasa.jpf.jvm.*;
import gov.nasa.jpf.jvm.bytecode.*;

/**
 * listener class to check for proper locking and unlocking
 */
public class LockUnlockListener extends PropertyListenerAdapter 
{
  int lockCount = 0;
  boolean propHolds = true;
 
  /********************** Property interface **************/
  public boolean check (VM vm, Object arg) {
    return propHolds;
  }
  
  public String getErrorMessage () { 
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    pw.print("lock-unlock property violated:\n");
  
    return sw.toString();
  }
  
  /********************** VMListener interface *************/
  public void instructionExecuted (VM v) {
    JVM vm=(JVM) v;
    Instruction insn = vm.getLastInstruction();    
    String instr = insn.toString();

    //if lock was called
    if(instr.indexOf("Lock") != -1) {
      ++lockCount;
      if(lockCount > 1) propHolds = false;
    }

    //if unlock was called
    if(instr.indexOf("Unlock") != -1) {
      --lockCount;
      if(lockCount < 0) propHolds = false;
    }

    //System.out.println(instr);
    //System.out.println(lockCount);
  }
 }
